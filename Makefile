ASM=nasm 
ASM_FLAGS=-f elf64
LD=ld
RESULT=run

.PHONY: help build clean

help:
	@echo "build				- Собрать программу"
	@echo "clean				- Удалить все "
	@exit 0

%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<


build: main.o lib.o dict.o
	$(LD) -o $(RESULT) $^

clean: clean
	rm $(RESULT) *.o


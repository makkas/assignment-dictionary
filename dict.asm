%define key_size 8
%include "lib.inc"

global find_word

section .text

; Принимает: Указатель на нуль-терминированную строку (rdi)
;   		 Указатель на начало словаря (rsi)
;  пройдёт по всему словарю в поисках подходящего ключа.
;  Если подходящее вхождение найдено, вернёт адрес *начала вхождения в   словарь* (не значения),
; иначе вернёт 0.
find_word:
	push rdi
	push rsi
	add rsi, key_size	;переместили указатель на ключ
	call string_equals	;сравниваем ключи
	pop rsi
	pop rdi
	cmp rax, 1			; ключ найден ? .found_key:
	je .found_key		

	mov rsi, [rsi]
   	test rsi, rsi		;Конец списка ? {rax = 0, ret} : find_word
	jne find_word		 
	xor rax, rax	
	ret			

.found_key:
	mov rax, rsi
	ret
	

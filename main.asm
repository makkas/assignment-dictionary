%define key_lenght 255
%define key_size 8

%include "lib.inc"
%include "colon.inc"


extern find_word
global _start


section .rodata
%include "words.inc"

input_text: db 'Input key, please: ', 0
error_input: db 'Max length of key is 255 symbols((', 0
error_find: db 'Not found for this key(', 0

section .bss
input_buffer: resb key_lenght+1



global _start


section .text

_start:
		mov rdi, input_text
		call print_string		; input_text -> stdout
		call print_newline

		mov rsi, key_lenght+1	
		mov rdi, input_buffer
		call read_string		; key <- stdin 
		test rax, rax			; неудача при вводе ключа
		je .error_input			
		
		mov rdi, input_buffer	; ищем слово 
		mov rsi, last_elem		
		call find_word			
		test rax, rax			; не нашли
		je .error_find			
		mov rdi, rax	
		add rdi, key_size		; нашли ? -> stdout
			push rdi
			call string_length		
			pop rdi
			inc rdi
			add rdi, rax
			call print_string		
			call print_newline
		mov rdi, 0				; 0-ой код возврата, всё норм
		jmp exit
	
	.error_input:
		mov rdi, error_input
		call print_error
		mov rdi, 2				; код возврата 2 -- ошибка чтения
		jmp exit
	.error_find:
		mov rdi, error_find
		call print_error
		mov rdi, 1				; код возврата 1 -- ключ в словаре не найден
		jmp exit
	

global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy

section .text
 
 
; Принимает код возврата (%rdi: int error_code) и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель (rdi) на нуль-терминированную строку, возвращает её длину
string_length:
    xor	rax, rax	                ; xor = 0
    .loop:				
        test byte[rdi + rax], 0xff	; Проверка на 0 текущего байта ; test -- выставляет флаги по операции поббитового сложения
        je	.end_loop		        ; je - переход, если ZF=1
        inc	rax			            ; Иначе rax++
        jmp	.loop			        ; jmp - безусловный переход
    .end_loop:		
    ret	           


; Принимает указатель (rdi) на нуль-терминированную строку, выводит её в stdout
print_string:
        mov r8, rdi		    ; сохраняем указатель в соответсвии с соглашанием вызовов
		call string_length	
		mov rdx, rax		; длина строки для syscall
		mov rax, 1			; Номер системного вызова
		mov rdi, 1			; Файловый дескриптор (1 - stdout)
		mov rsi, r8		    ; адресс строки для syscall
		syscall
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr и переводит строку
print_error:
        mov r8, rdi		    ; сохраняем указатель в соответсвии с соглашанием вызовов
		call string_length	
		mov rdx, rax		; длина строки для syscall
		mov rax, 1			; Номер системного вызова
		mov rdi, 2			; Файловый дескриптор (2 - stderr)
		mov rsi, r8		    ; адресс строки для syscall
		syscall

		push 0xA			; переводим строку
			mov rax, 1			; Номер системного вызова
			mov rdi, 2			; Файловый дескриптор (2 - stderr)
			mov rsi, rsp		; адресс строки для syscall (вершина стека, печатаем первый символ со стека)
			mov rdx, 1		    ; длина строки для syscall
			syscall
		pop rdi
		ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    	mov rax, 1			; Номер системного вызова
		mov rdi, 1			; Файловый дескриптор (1 - stdout)
		mov rsi, rsp		; адресс строки для syscall (вершина стека, печатаем первый символ со стека)
		mov rdx, 1		    ; длина строки для syscall
        syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char          ; не хочется зависеть от порядка подпрограмм, для облегчения изменений


; Выводит беззнаковое 8-байтовое число (rdi) в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rcx, 20		    	    ; максимальная длина строки (2**64 -> 20 символов) (кол-во свободных байт)
    sub rsp, 21		    	    ; (+1 символ нуль-терминатора) ; выделяем место на стеке
    mov r8, 10		    	    ; делитель = основание резулбтирующей с.с.
    mov rax, rdi	    	    ; исходное число -> делимое для div
    mov byte[rsp + rcx], 0	    ; Кладем в конец строки нуль-терминатор
    .loop:
        xor rdx, rdx		    ; Чистим остаток от div 
        dec rcx			        ; (кол-во свободных байт)--
        div r8			        ; Получаем очередную десятичную цифру выводимого числа ; div -- делит rax на аргумент;  
        mov byte[rsp + rcx], dl	; dl -- младший байт rdx -- остатка div
        add byte[rsp + rcx], '0'; цифра -> символ
        test rax, rax		    ; число закончилось?
    jne .loop			        ; ZF==0 ? .loop

    mov rdi, rsp		        ; Указатель на строку -> rdi: rsp + кол-во свободных байт
    add rdi, rcx		        
    call print_string     
    add rsp, 21                 ; указатель стека <- адрес возврата
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi		
    jge print_uint		; X>=0 ? print_uint :
    neg rdi			    ; x = -x
    push rdi		    ; x -> rsi

    mov rdi, '-'		; print '-'
    call print_char		
    
    pop rdi             ; print_uint(x)
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки (rdi, rsi), возвращает (rax) 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
	xor rax, rax
    .loop:
    	mov dl, byte[rdi + rcx]
    	cmp dl, byte[rsi + rcx]
    	jne .notEqual     ; ZF!=0 ? .isNotEqual
    	inc rcx
    	test dl, dl       ; dl==0 ? .equal
    	je .equal
    	jmp .loop
    .equal:
    	mov rax, 1
    	ret
    .notEqual:
    	mov rax, 0
    	ret


; Читает один символ из stdin и возвращает его. Возвращает (rax) 0 если достигнут конец потока
read_char:
    push 0         ; место для символа (значение по умолчанию, если ничего не будет записано - конец потока)

    xor rax, rax   ; Номер системного вызова.
    xor rdi, rdi   ; Файловый дескриптор (0 - ст-й поток ввода).
    mov rsi, rsp   ; адрес для записи
    mov rdx, 1     ; кол-во байт
    syscall 

    pop rax        ; достаём символ в rax
    ret 

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12							; используем r12-r14, чтобы не думать про изменяемость в вызываемых подпрограммах
    push r13
    push r14
    mov r12, rdi						; указатель буфера
    mov r13, rsi						; размер буфера
    dec r13                             ; уменьшаем буффер искуственно на 1 для 0-терминатора
    xor r14, r14						; кол-во считанных не пробельных символов
    cmp r13, 0                          ; r13 < 0 ? .fail_finish ; полезный размер буффера должен быть неотрицательным
    jl .fail_finish
    .loop:
        call read_char						
        cmp rax, 0x20						; пробел
        je .first_whitespace_symbol		
        cmp rax, 0x9						; табуляция
        je .first_whitespace_symbol
        cmp rax, 0xA						; перевод строки
        je .first_whitespace_symbol
        test rax, rax						; конец потока ? .fail_finish
        je .eof_is_first_symbol
    
        inc r14								; (кол-во считанных не пробельных символов)++
        cmp r14, r13						; кол-во считанных не пробельных символов > размер буфера ? .fail_finish
        jg .fail_finish					
        mov byte[r12 + r14 - 1], al			; символ ( al = младший байт rax ) -> буфер 
        jmp .loop
    .eof_is_first_symbol:                   ; кол-во считанных не пробельных символов == 0 ? .fail_finish : .finish
        test r14, r14   
        je .fail_finish
        jne .finish
    .first_whitespace_symbol:                     ; (кол-во считанных не пробельных символов == 0 ? .loop : .finish)
        test r14, r14
        je .loop
    .finish:
        mov byte[r12 + r14], 0		    	; 0-терминатор
        mov rax, r12						; указатель буфера -> rax
        mov rdx, r14						; кол-во считанных не пробельных символов -> rdx
        jmp .return
    .fail_finish:
        xor rax, rax
        xor rdx, rdx
    .return:
        pop r14 							; возвращаем r12-r14
        pop r13
        pop r12
        ret

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер строку из stdin (до перевода строки или конца ввода)
; Останавливается и возвращает 0 если строка слишком большая для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
 read_string:
    push r12							; используем r12-r14, чтобы не думать про изменяемость в вызываемых подпрограммах
    push r13
    push r14
    mov r12, rdi						; указатель буфера
    mov r13, rsi						; размер буфера
    dec r13                             ; уменьшаем буффер искуственно на 1 для 0-терминатора
    xor r14, r14						; кол-во считанных символов
    cmp r13, 0                          ; r13 < 0 ? .fail_finish ; полезный размер буффера должен быть неотрицательным
    jl .fail_finish
    .loop:
        call read_char
		cmp rax, 0xA						; перевод строки || конец потока ? .fail_finish
        je .finish						
        test rax, rax						
        je .finish
    
        inc r14								; (кол-во считанных символов)++
        cmp r14, r13						; кол-во считанных символов > размер буфера ? .fail_finish
        jg .fail_finish					
        mov byte[r12 + r14 - 1], al			; символ ( al = младший байт rax ) -> буфер 
        jmp .loop
    .finish:
        mov byte[r12 + r14], 0		    	; 0-терминатор
        mov rax, r12						; указатель буфера -> rax
        mov rdx, r14						; кол-во считанных символов -> rdx
        jmp .return
    .fail_finish:
        xor rax, rax
        xor rdx, rdx
    .return:
        pop r14 							; возвращаем r12-r14
        pop r13
        pop r12
        ret



; Принимает указатель на строку (rdi), пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor rcx, rcx		; длина числа в символах
        mov r9, 10			; Основание системы счисления = множитель
        xor r8, r8			; временный для символов
        xor rax, rax		; результат
        mov r8b, byte[rdi]	; берём первый символ
        sub r8b, '0'		; символ -> цифру
        jl	.fail_finish	; r8b<0 || r8b>9 ? .fail_finish
        cmp r8b, 9			
        jg	.fail_finish	
        add rax, r8			; добавляем к результату
        inc rcx			    
        test rax, rax		; первая цифра == 0 ? .finish
        je	.finish			 
    .loop: 
        mov r8b, byte[rdi + rcx]	; берём очередной символ
        sub r8b, '0'		; символ -> цифру 
        jl	.finish			; r8b<0 || r8b>9 ? .finish
        cmp r8b, 9			
        jg	.finish			
        mul r9			    ; сдвигаем разряд ; rax *= r9
        add rax, r8			; добавляем к результату
        inc rcx			    
        jmp .loop			
    .fail_finish:			
        xor rdx, rdx		
        ret
    .finish:
        mov rdx, rcx
        ret				

; Принимает указатель на строку (rdi), пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
        mov r8b, byte[rdi]		; берём первый символ
        cmp r8b, '-'		    ; symbol == '-' || symbol== '+' ? .sign
        je .sign			
        cmp r8b, '+'		    
        je .sign
        jmp parse_uint	        ; ответом будет parse_uint	    
    .sign:
        push rdi        ; сохраняем указатель
        inc rdi
        call parse_uint ; parse_uint со следующего символа
        pop rdi         ; возвращаем указатель и стек
        test rdx, rdx   ; rdx==0?finish (parse_uint faid ? .finish)
        je .finish
        inc rdx         ; добавляем знак
        cmp byte[rdi], '-' ; sign == '-' ? neg rax : .finish
        jne .finish
        neg rax
    .finish:
        ret


; Принимает указатель на строку (rdi), указатель на буфер (rsi) и длину буфера (rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length      
    inc rax                 ; string_length()++ для нуль терминатора
    cmp rdx, rax            
    jl .fail_finish         ; rdx < ++string_length() ? error 
    xor rax, rax
    .loop:
        mov dl, byte[rdi+rax] 
        mov byte[rsi+rax], dl
        inc rax
        test dl, dl ; dl!=0 ? .loop 
        jne .loop
    ret
    .fail_finish:
        xor rax, rax
        ret

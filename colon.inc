%define last_elem 0

%macro colon 2
    %ifid %2
        %2: dq last_elem
    %else
        %fatal "ERROR: identifier expected as second argument"
    %endif
    %ifstr %1
        %%elem_str: db %1, 0
    %else
        %fatal "ERROR: string expected as first argument"
    %endif
    %define last_elem %2
%endmacro
